<?php
/**
 * @file
 * lost_pet.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function lost_pet_taxonomy_default_vocabularies() {
  return array(
    'cities' => array(
      'name' => 'Cities',
      'machine_name' => 'cities',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
