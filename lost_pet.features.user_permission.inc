<?php
/**
 * @file
 * lost_pet.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function lost_pet_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create lost_pet content'.
  $permissions['create lost_pet content'] = array(
    'name' => 'create lost_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any lost_pet content'.
  $permissions['delete any lost_pet content'] = array(
    'name' => 'delete any lost_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own lost_pet content'.
  $permissions['delete own lost_pet content'] = array(
    'name' => 'delete own lost_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any lost_pet content'.
  $permissions['edit any lost_pet content'] = array(
    'name' => 'edit any lost_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own lost_pet content'.
  $permissions['edit own lost_pet content'] = array(
    'name' => 'edit own lost_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag pet_found'.
  $permissions['flag pet_found'] = array(
    'name' => 'flag pet_found',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'publish button publish any lost_pet'.
  $permissions['publish button publish any lost_pet'] = array(
    'name' => 'publish button publish any lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable lost_pet'.
  $permissions['publish button publish editable lost_pet'] = array(
    'name' => 'publish button publish editable lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own lost_pet'.
  $permissions['publish button publish own lost_pet'] = array(
    'name' => 'publish button publish own lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any lost_pet'.
  $permissions['publish button unpublish any lost_pet'] = array(
    'name' => 'publish button unpublish any lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable lost_pet'.
  $permissions['publish button unpublish editable lost_pet'] = array(
    'name' => 'publish button unpublish editable lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own lost_pet'.
  $permissions['publish button unpublish own lost_pet'] = array(
    'name' => 'publish button unpublish own lost_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'unflag pet_found'.
  $permissions['unflag pet_found'] = array(
    'name' => 'unflag pet_found',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
