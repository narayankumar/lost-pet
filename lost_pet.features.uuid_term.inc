<?php
/**
 * @file
 * lost_pet.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function lost_pet_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Hyderabad',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '075ac33f-0183-4935-b8c9-e9b889f0f2ec',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Other',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2e23181c-51dc-4990-8d26-a10b5c11fbdf',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Chennai',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2f62d9eb-85d5-4463-beb4-334fbde33342',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Kolkata',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '325fb513-ee38-4227-8ab9-5caae233a313',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Thane',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '86190920-0d90-4f76-9ab4-e8df96abccd2',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'New Delhi',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '963d5838-10e2-46d1-8d51-5cf22aaadc4e',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pune',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '9fba3036-99ca-458c-b137-61b0e0cd9956',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Bangalore',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a4d9cd3b-0f74-4a9a-ae6a-62b228ad568c',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Ahmedabad',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'd0ac58ad-4aba-4bff-9e52-4c260ee60f51',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Mumbai',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f6223bd3-4b9a-4a86-b826-d40e2bf41298',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Gurgaon',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f82ce54c-5432-4ce3-933d-681a5b8b6b99',
    'vocabulary_machine_name' => 'cities',
    'metatags' => array(),
  );
  return $terms;
}
