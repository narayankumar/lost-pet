
build2014092102
- duplicate cities deleted in taxonomy

build2014092101
- image field made compulsory
- removed 'more' link in page manager from lost-found page
- rules message changed to 're-united with owner'
- removed wrong field from teasers view, replaced with pet-found field
- search by city exposed filter added to teasers view

build2014091801
- made title 'pets lost' in teasers view
- gave it class=back, h4, second word strong
- teasers view - found message as red & strong 
- changed link wording to 'report lost or missing pet'

build2014091602
- provide comments box 
- hide share this label

7.x-1.0-dev1
- initial commit on 12 sep 2014
